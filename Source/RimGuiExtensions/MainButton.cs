using RimWorld;
using UnityEngine;
using Verse;

namespace PorousBoat.RimGuiExtensions
{
    public class MainButtonWorker_ExtendedToggleTab : MainButtonWorker_ToggleTab
    {
    }

    public class MainButtonWorker_ExtendedToggleTabWorld : MainButtonWorker_ToggleWorld
    {
    }
    public class MainButtonWorker_ExtendedToggleTabResearch : MainButtonWorker_ToggleResearchTab
    {
    }
}