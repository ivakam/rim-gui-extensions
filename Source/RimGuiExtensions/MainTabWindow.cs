using System;
using System.Collections.Generic;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;

namespace PorousBoat.RimGuiExtensions
{
    public class ExtendedTabWindow_Work : MainTabWindow_Work
    {
    }
    
    public class ExtendedTabWindow_Schedule : MainTabWindow_Schedule
    {
    }
    
    public class ExtendedTabWindow_Assign : MainTabWindow_Assign
    {
    }
    
    public class ExtendedTabWindow_Animals : MainTabWindow_Animals
    {
    }
    
    public class ExtendedTabWindow_Wildlife : MainTabWindow_Wildlife
    {
        protected override void SetInitialSizeAndPosition()
        {
            base.SetInitialSizeAndPosition();
            windowRect.x = 0;
            windowRect.y = Math.Max(UI.screenHeight - windowRect.height, 0);
        }
    }
    
    public class ExtendedTabWindow_Architect : MainTabWindow_Architect
    {
        protected override void SetInitialSizeAndPosition()
        {
            base.SetInitialSizeAndPosition();
            windowRect.x = 0;
            windowRect.y = (UI.screenHeight - RimGuiExtensions.Settings.tabWindowBottomMargin) - windowRect.height;
        }
    }

    public class ExtendedTabWindow_Quests : MainTabWindow_Quests
    {
    }

    public class ExtendedTabWindow_Research : MainTabWindow_Research
    {
        protected override void SetInitialSizeAndPosition()
        {
            base.SetInitialSizeAndPosition();
            windowRect.x = 0;
            windowRect.y = (UI.screenHeight - RimGuiExtensions.Settings.tabWindowBottomMargin) - windowRect.height;
        }
    }

    public class ExtendedTabWindow_Ideos : MainTabWindow_Ideos
    {
        public override Vector2 InitialSize =>
            new Vector2(base.InitialSize.x, UI.screenHeight - RimGuiExtensions.Settings.tabWindowBottomMargin);
    }

    public class ExtendedTabWindow_History : MainTabWindow_History
    {
    }

    public class ExtendedTabWindow_Inspect : MainTabWindow_Inspect
    {
        protected override void SetInitialSizeAndPosition()
        {
            base.SetInitialSizeAndPosition();
            windowRect.x = 0;
            //Fix the damn 35
            windowRect.y = UI.screenHeight - windowRect.height - 35f;
        }
    }

    public class ExtendedTabWindow_Factions : MainTabWindow_Factions
    {
    }

    public class ExtendedTabWindow_Menu : MainTabWindow_Menu
    {
        protected override void SetInitialSizeAndPosition()
        {
            base.SetInitialSizeAndPosition();
            Log.Message("Calling menu override");
            windowRect.x = UI.screenWidth - windowRect.width;
            windowRect.y = (UI.screenHeight - RimGuiExtensions.Settings.tabWindowBottomMargin) - windowRect.height;
        }
    }
}