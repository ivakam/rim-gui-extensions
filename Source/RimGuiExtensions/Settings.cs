using RimWorld;
using UnityEngine;
using Verse;

namespace PorousBoat.RimGuiExtensions
{
    public class Settings : ModSettings
    {
        public float tabWindowBottomMargin = 88f;
        
        public int dockHeight = 256;
        public int dockPadding = 16;
        public int dockOffsetY = 50;
        
        public float mainButtonWidth = 64f;
        public float mainBottomMargin = 12f;
        public int mainButtonMargin = 10;
        
        public float iconOffset = 16f;
        public float iconSize = 32f;
        
        public float gizmoStartX = 210f;
        public float gizmoHeight = 75f;
        public int gizmoRightMargin = 147;
        
        public bool useColorForIcons = false;
        public bool useBorderForSmallIcons = false;
        public bool alwaysSortMinimizedLast = true;

        public void DoWindowContents(Rect canvas)
        {
            Dialog_Options.DoOptions(canvas);
        }

        public void ResetSettings()
        {
            tabWindowBottomMargin = 88f;
            
            dockHeight = 256;
            dockPadding = 16;
            dockOffsetY = 50;
            
            mainButtonWidth = 64f;
            mainBottomMargin = 12f;
            mainButtonMargin = 10;
            
            iconOffset = 16f;
            iconSize = 32f;
            
            gizmoStartX = 210f;
            gizmoRightMargin = 147;
            gizmoHeight = 75f;
            
            useColorForIcons = false;
            useBorderForSmallIcons = false;
            alwaysSortMinimizedLast = true;
        }
    }
}