﻿using System.Collections.Generic;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace PorousBoat.RimGuiExtensions
{
    [StaticConstructorOnStartup]
    public class RimGuiExtensions : Mod
    {
        public static Harmony harmonyInstance;
        
        private static RimGuiExtensions _instance;
        public static Settings Settings => ((Mod) _instance).GetSettings<Settings>();
        public static Dialog_Options optionsWindow;
        
        public RimGuiExtensions(ModContentPack content) : base(content)
        {
            Log.Message("GUI Extensions: Running...");
            harmonyInstance = new Harmony("PorousBoat.RimGuiExtensions");
            harmonyInstance.PatchAll();
            _instance = this;
        }
        
        public static bool MainIconButton(Rect rect)
        {
            Vector2 center = rect.center;
            float buttonSize = rect.width;
            float offset = buttonSize / 2;
            GUI.color = Color.white;
            
            if (Mouse.IsOver(rect))
            {
                GUI.color = GenUI.MouseoverColor;
            }
            //Draw button fill
            Texture2D buttonTex = ContentFinder<Texture2D>.Get("UI/Buttons/Button_fill");
            GUI.DrawTexture(new Rect(center.x - offset, center.y - offset, buttonSize, buttonSize), buttonTex);
            //Draw button outline
            if (Settings.useBorderForSmallIcons || buttonSize >= RimGuiExtensions.Settings.mainButtonWidth)
            {
                buttonTex = ContentFinder<Texture2D>.Get("UI/Buttons/Button_outline");
                GUI.color = Faction.OfPlayer.ideos.PrimaryIdeo.Color;
                GUI.DrawTexture(new Rect(center.x - offset, center.y - offset, buttonSize, buttonSize), buttonTex);
                GUI.color = Color.white;
            }
            MouseoverSounds.DoRegion(rect, SoundDefOf.Mouseover_Category);
            return Widgets.ButtonInvisible(rect, false);
        }
        
        public override void DoSettingsWindowContents(Rect canvas)
        {
            Settings.DoWindowContents(canvas);
        }

        public override string SettingsCategory()
        {
            return "BetterUI";
        }
    }

    public static class RimGuiUtils
    {
        public static int CenterOnScreenX(int width)
        {
            return (UI.screenWidth / 2) - (width / 2);
        }
        
        public static int CenterOnScreenY(int height)
        {
            return (UI.screenHeight / 2) - (height / 2);
        }

        public static void Move<T>(ref List<T> list, int oldIndex, int newIndex)
        {
            T item = list[oldIndex];
            list.RemoveAt(oldIndex);
            list.Insert(newIndex, item);
        }

        public static Color getAccentColor()
        {
            if (ModsConfig.IdeologyActive)
            {
                return Faction.OfPlayer.ideos.PrimaryIdeo.Color;
            }
            else
            {
                return Faction.OfPlayer.Color;
            }
        }
    }
}