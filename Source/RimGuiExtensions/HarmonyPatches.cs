using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;

namespace PorousBoat.RimGuiExtensions
{
    [HarmonyPatch(typeof(MainButtonsRoot), "DoButtons")]
    public class Patch_MainButtonsRoot
    {
        static bool Prefix()
        {
            return false;
        }

        static void Postfix(List<MainButtonDef> ___allButtonsInOrder)
        {
            float buttonWidth = RimGuiExtensions.Settings.mainButtonWidth;
            float smallButtonWidth = buttonWidth / 2;
            int buttonMargin = RimGuiExtensions.Settings.mainButtonMargin;
            float bottomMargin = RimGuiExtensions.Settings.mainBottomMargin;
            float initialDockWidth = buttonMargin;

	            
            // Count minimized buttons as half length
            for (int i = 0; i < ___allButtonsInOrder.Count; i++)
            {
                if (___allButtonsInOrder[i].buttonVisible)
                {
                    initialDockWidth += buttonMargin + (buttonWidth * (___allButtonsInOrder[i].minimized ? 0.5f : 1f));
                }
            }
            GUI.color = Color.white;
            //int finalButtonIndex = ___allButtonsInOrder.FindLastIndex(x => x.buttonVisible);
            int offset = (int)((UI.screenWidth / 2) - (initialDockWidth / 2));
            
            //Draw dock
            int dockOffsetY = RimGuiExtensions.Settings.dockOffsetY;
            int dockPadding = RimGuiExtensions.Settings.dockPadding;
            initialDockWidth += dockPadding * 2;
            int dockHeight = RimGuiExtensions.Settings.dockHeight;
            //Dock fill
            Texture2D dockTex = ContentFinder<Texture2D>.Get("UI/Dock_fill");
            Rect dockRect = new Rect(offset - dockPadding, UI.screenHeight - dockOffsetY, initialDockWidth, dockHeight);
            GUI.DrawTexture(dockRect, dockTex);
            //Dock outline
			GUI.color = Faction.OfPlayer.ideos.PrimaryIdeo.Color;
            dockTex = ContentFinder<Texture2D>.Get("UI/Dock_outline");
            dockRect = new Rect(offset - dockPadding, UI.screenHeight - dockOffsetY, initialDockWidth, dockHeight);
            GUI.DrawTexture(dockRect, dockTex);
            GUI.color = Color.white;
            
            offset += buttonMargin;
            for (int j = 0; j < ___allButtonsInOrder.Count; j++)
            {
	            if (___allButtonsInOrder[j].minimized && j != ___allButtonsInOrder.Count - 1 && !___allButtonsInOrder[j+1].minimized)
	            {
		            RimGuiUtils.Move(ref ___allButtonsInOrder, j, j + 1);
	            }
                if (___allButtonsInOrder[j].buttonVisible)
                {
                    int currWidth = (int)buttonWidth;
                    int smallExtraOffset = 0;
                    if (___allButtonsInOrder[j].minimized)
                    {
                        currWidth = (int)smallButtonWidth;
                        smallExtraOffset = 6;
                    }
                    float centerCompensation = buttonWidth - currWidth + smallExtraOffset;
                    
                    Rect rect = new Rect(offset, UI.screenHeight - bottomMargin - buttonWidth + centerCompensation, currWidth, currWidth);
                    ___allButtonsInOrder[j].Worker.DoButton(rect);
                    offset += currWidth + buttonMargin;
                }
            }
            
        }
    }

    [HarmonyPatch(typeof(MainButtonWorker), "DoButton")]
    public class Patch_MainButtonWorker
    {
	    static bool Prefix()
	    {
		    return false;
	    }

	    static void Postfix(Rect rect, MainButtonWorker __instance)
	    {
            Text.Font = GameFont.Small;
            float num = __instance.def.LabelCapWidth;
            if (__instance.Disabled)
            {
                Widgets.DrawAtlas(rect, Widgets.ButtonSubtleAtlas);
                if (Event.current.type == EventType.MouseDown && Mouse.IsOver(rect))
                {
                    Event.current.Use();
                }
                return;
            }
            Rect rect2 = rect;
            if (RimGuiExtensions.MainIconButton(rect2))
            {
                __instance.InterfaceTryActivate();
            }
            if (__instance.def.Icon != null)
            {
                Vector2 center = rect.center;
                float num2 = RimGuiExtensions.Settings.iconOffset;
                Color oldColor = GUI.color;
                if (RimGuiExtensions.Settings.useColorForIcons)
                {
                    GUI.color = RimGuiUtils.getAccentColor();
                }
                float size = RimGuiExtensions.Settings.iconSize;
                GUI.DrawTexture(new Rect(center.x - num2, center.y - num2, size, size), __instance.def.Icon);
                GUI.color = oldColor;
            }
            if (Find.MainTabsRoot.OpenTab != __instance.def && !Find.WindowStack.NonImmediateDialogWindowOpen)
            {
                UIHighlighter.HighlightOpportunity(rect, __instance.def.cachedHighlightTagClosed);
            }
            if (Mouse.IsOver(rect) && !__instance.def.description.NullOrEmpty())
            {
                TooltipHandler.TipRegion(rect, __instance.def.LabelCap + "\n\n" + __instance.def.description);
            }
	    }
    }
    
    [HarmonyPatch(typeof(ArchitectCategoryTab), "DesignationTabOnGUI")]
    public class Patch_ArchitectCategoryTab
    {
        static bool Prefix()
        {
            return false;
        }

        static void Postfix(ArchitectCategoryTab __instance,
            Designator forceActivatedCommand,
            DesignationCategoryDef ___def,
            Func<Gizmo, bool> ___shouldLowLightGizmoFunc,
            Func<Gizmo, bool> ___shouldHighLightGizmoFunc)
        {
            if (Find.DesignatorManager.SelectedDesignator != null)
            {
                Find.DesignatorManager.SelectedDesignator.DoExtraGuiControls(0f, (UI.screenHeight - RimGuiExtensions.Settings.tabWindowBottomMargin) - ((MainTabWindow_Architect)MainButtonDefOf.Architect.TabWindow).WinHeight - 270f);
            }
            Func<Gizmo, bool> customActivatorFunc = ((forceActivatedCommand == null) ? null : ((Func<Gizmo, bool>)((Gizmo cmd) => cmd == forceActivatedCommand)));
            float startX = RimGuiExtensions.Settings.gizmoStartX;
            GizmoGridDrawer.DrawGizmoGrid(___def.ResolvedAllowedDesignators, startX, out var mouseoverGizmo, customActivatorFunc, ___shouldHighLightGizmoFunc, ___shouldLowLightGizmoFunc);
            if (mouseoverGizmo == null && Find.DesignatorManager.SelectedDesignator != null)
            {
                mouseoverGizmo = Find.DesignatorManager.SelectedDesignator;
            }

            Traverse t = Traverse.Create(__instance);
            Rect InfoRect = t.Property("InfoRect").GetValue<Rect>();
            InfoRect.y += 35 - RimGuiExtensions.Settings.tabWindowBottomMargin;
            t.Method("DoInfoBox", InfoRect, (Designator)mouseoverGizmo).GetValue();
        }
    }

    [HarmonyPatch(typeof(GizmoGridDrawer), "DrawGizmoGrid")]
    public static class Patch_GizmoGridDrawer
    {
	    static bool Prefix()
	    {
		    return false;
	    }

	    static void Postfix(IEnumerable<Gizmo> gizmos,
		    float startX,
		    out Gizmo mouseoverGizmo,
		    Func<Gizmo, bool> customActivatorFunc,
		    Func<Gizmo, bool> highlightFunc,
		    Func<Gizmo, bool> lowlightFunc,
		    List<Gizmo> ___tmpAllGizmos,
		    List<Gizmo> ___firstGizmos,
		    List<Command> ___shrinkableCommands,
		    List<List<Gizmo>> ___gizmoGroups,
		    Func<Gizmo, Gizmo, int> ___SortByOrder,
		    Func<Gizmo, bool> ___customActivator,
		    Vector2 ___GizmoSpacing,
		    float ___heightDrawn,
		    int ___heightDrawnFrame,
		    HashSet<KeyCode> ___drawnHotKeys)
	    {
		    if (Event.current.type == EventType.Layout)
		    {
			    mouseoverGizmo = null;
			    return;
		    }
		    
		    ___tmpAllGizmos.Clear();
		    ___tmpAllGizmos.AddRange(gizmos);
		    ___tmpAllGizmos.SortStable(___SortByOrder);
		    ___gizmoGroups.Clear();
		    for (int i = 0; i < ___tmpAllGizmos.Count; i++)
		    {
			    Gizmo gizmo = ___tmpAllGizmos[i];
			    bool flag = false;
			    for (int j = 0; j < ___gizmoGroups.Count; j++)
			    {
				    if (___gizmoGroups[j][0].GroupsWith(gizmo))
				    {
					    flag = true;
					    ___gizmoGroups[j].Add(gizmo);
					    ___gizmoGroups[j][0].MergeWith(gizmo);
					    break;
				    }
			    }

			    if (!flag)
			    {
				    List<Gizmo> list = SimplePool<List<Gizmo>>.Get();
				    list.Add(gizmo);
				    ___gizmoGroups.Add(list);
			    }
		    }

		    ___firstGizmos.Clear();
		    ___shrinkableCommands.Clear();
		    int gizmoRightMargin = RimGuiExtensions.Settings.gizmoRightMargin;
		    float gizmoHeight = RimGuiExtensions.Settings.gizmoHeight;
		    float num = UI.screenWidth - gizmoRightMargin;
		    Vector2 vector = new Vector2(startX, (UI.screenHeight - RimGuiExtensions.Settings.tabWindowBottomMargin) - ___GizmoSpacing.y - gizmoHeight);
		    float maxWidth = num - startX;
		    int num2 = 0;
		    for (int k = 0; k < ___gizmoGroups.Count; k++)
		    {
			    List<Gizmo> list2 = ___gizmoGroups[k];
			    Gizmo gizmo2 = null;
			    for (int l = 0; l < list2.Count; l++)
			    {
				    if (!list2[l].disabled)
				    {
					    gizmo2 = list2[l];
					    break;
				    }
			    }

			    if (gizmo2 == null)
			    {
				    gizmo2 = list2.FirstOrDefault();
			    }
			    else
			    {
				    Command_Toggle command_Toggle = gizmo2 as Command_Toggle;
				    if (command_Toggle != null)
				    {
					    if (!command_Toggle.activateIfAmbiguous && !command_Toggle.isActive())
					    {
						    for (int m = 0; m < list2.Count; m++)
						    {
							    Command_Toggle command_Toggle2 = list2[m] as Command_Toggle;
							    if (command_Toggle2 != null && !command_Toggle2.disabled && command_Toggle2.isActive())
							    {
								    gizmo2 = list2[m];
								    break;
							    }
						    }
					    }

					    if (command_Toggle.activateIfAmbiguous && command_Toggle.isActive())
					    {
						    for (int n = 0; n < list2.Count; n++)
						    {
							    Command_Toggle command_Toggle3 = list2[n] as Command_Toggle;
							    if (command_Toggle3 != null && !command_Toggle3.disabled && !command_Toggle3.isActive())
							    {
								    gizmo2 = list2[n];
								    break;
							    }
						    }
					    }
				    }
			    }

			    if (gizmo2 != null)
			    {
				    Command command;
				    if ((command = gizmo2 as Command) != null && command.shrinkable && command.Visible)
				    {
					    ___shrinkableCommands.Add(command);
				    }

				    if (vector.x + gizmo2.GetWidth(maxWidth) > num)
				    {
					    vector.x = startX;
					    vector.y -= gizmoHeight + ___GizmoSpacing.x;
					    num2++;
				    }

				    vector.x += gizmo2.GetWidth(maxWidth) + ___GizmoSpacing.x;
				    ___firstGizmos.Add(gizmo2);
			    }
		    }
		    if (num2 > 1 && ___shrinkableCommands.Count > 1)
		    {
			    for (int num3 = 0; num3 < ___shrinkableCommands.Count; num3++)
			    {
				    ___firstGizmos.Remove(___shrinkableCommands[num3]);
			    }
		    }
		    else
		    {
			    ___shrinkableCommands.Clear();
		    }

		    ___drawnHotKeys.Clear();
		    ___customActivator = customActivatorFunc;
		    Text.Font = GameFont.Tiny;
		    Vector2 vector2 = new Vector2(startX, (UI.screenHeight - RimGuiExtensions.Settings.tabWindowBottomMargin) - ___GizmoSpacing.y - gizmoHeight);
		    mouseoverGizmo = null;
		    Gizmo interactedGiz = null;
		    Event interactedEvent = null;
		    Gizmo floatMenuGiz = null;
		    for (int num4 = 0; num4 < ___firstGizmos.Count; num4++)
		    {
			    Gizmo gizmo3 = ___firstGizmos[num4];
			    if (gizmo3.Visible)
			    {
				    if (vector2.x + gizmo3.GetWidth(maxWidth) > num)
				    {
					    vector2.x = startX;
					    vector2.y -= gizmoHeight + ___GizmoSpacing.x;
				    }

				    ___heightDrawnFrame = Time.frameCount;
				    ___heightDrawn = (float) UI.screenHeight - vector2.y;
				    GizmoRenderParms parms = default(GizmoRenderParms);
				    parms.highLight = highlightFunc?.Invoke(gizmo3) ?? false;
				    parms.lowLight = lowlightFunc?.Invoke(gizmo3) ?? false;
				    GizmoResult result2 = gizmo3.GizmoOnGUI(vector2, maxWidth, parms);
				    ProcessGizmoState(gizmo3, result2, ref mouseoverGizmo);
				    GenUI.AbsorbClicksInRect(
					    new Rect(vector2.x, vector2.y, gizmo3.GetWidth(maxWidth), gizmoHeight + ___GizmoSpacing.y)
						    .ContractedBy(-12f));
				    vector2.x += gizmo3.GetWidth(maxWidth) + ___GizmoSpacing.x;
			    }
		    }

		    float x = vector2.x;
		    int num5 = 0;
		    for (int num6 = 0; num6 < ___shrinkableCommands.Count; num6++)
		    {
			    Command command2 = ___shrinkableCommands[num6];
			    float getShrunkSize = command2.GetShrunkSize;
			    if (vector2.x + getShrunkSize > num)
			    {
				    num5++;
				    if (num5 > 1)
				    {
					    x = startX;
				    }

				    vector2.x = x;
				    vector2.y -= getShrunkSize + 3f;
			    }

			    Vector2 topLeft = vector2;
			    topLeft.y += getShrunkSize + 3f;
			    ___heightDrawnFrame = Time.frameCount;
			    ___heightDrawn = Mathf.Min(___heightDrawn, (float) UI.screenHeight - topLeft.y);
			    GizmoRenderParms parms2 = default(GizmoRenderParms);
			    parms2.highLight = highlightFunc?.Invoke(command2) ?? false;
			    parms2.lowLight = lowlightFunc?.Invoke(command2) ?? false;
			    GizmoResult result3 = command2.GizmoOnGUIShrunk(topLeft, getShrunkSize, parms2);
			    ProcessGizmoState(command2, result3, ref mouseoverGizmo);
			    GenUI.AbsorbClicksInRect(
				    new Rect(topLeft.x, topLeft.y, getShrunkSize, getShrunkSize + 3f).ExpandedBy(3f));
			    vector2.x += getShrunkSize + 3f;
		    }

		    if (interactedGiz != null)
		    {
			    List<Gizmo> list3 = FindMatchingGroup(___gizmoGroups, interactedGiz);
			    for (int num7 = 0; num7 < list3.Count; num7++)
			    {
				    Gizmo gizmo4 = list3[num7];
				    if (gizmo4 != interactedGiz && !gizmo4.disabled && interactedGiz.InheritInteractionsFrom(gizmo4))
				    {
					    gizmo4.ProcessInput(interactedEvent);
				    }
			    }

			    interactedGiz.ProcessInput(interactedEvent);
			    Event.current.Use();
		    }
		    else if (floatMenuGiz != null)
		    {
			    List<FloatMenuOption> list4 = new List<FloatMenuOption>();
			    foreach (FloatMenuOption rightClickFloatMenuOption in floatMenuGiz.RightClickFloatMenuOptions)
			    {
				    list4.Add(rightClickFloatMenuOption);
			    }

			    List<Gizmo> list5 = FindMatchingGroup(___gizmoGroups, floatMenuGiz);
			    for (int num8 = 0; num8 < list5.Count; num8++)
			    {
				    Gizmo gizmo5 = list5[num8];
				    if (gizmo5 == floatMenuGiz || gizmo5.disabled ||
				        !floatMenuGiz.InheritFloatMenuInteractionsFrom(gizmo5))
				    {
					    continue;
				    }

				    foreach (FloatMenuOption rightClickFloatMenuOption2 in gizmo5.RightClickFloatMenuOptions)
				    {
					    FloatMenuOption floatMenuOption = null;
					    for (int num9 = 0; num9 < list4.Count; num9++)
					    {
						    if (list4[num9].Label == rightClickFloatMenuOption2.Label)
						    {
							    floatMenuOption = list4[num9];
							    break;
						    }
					    }

					    if (floatMenuOption == null)
					    {
						    list4.Add(rightClickFloatMenuOption2);
					    }
					    else
					    {
						    if (rightClickFloatMenuOption2.Disabled)
						    {
							    continue;
						    }

						    if (!floatMenuOption.Disabled)
						    {
							    Action prevAction = floatMenuOption.action;
							    Action localOptionAction = rightClickFloatMenuOption2.action;
							    floatMenuOption.action = delegate
							    {
								    prevAction();
								    localOptionAction();
							    };
						    }
						    else if (floatMenuOption.Disabled)
						    {
							    list4[list4.IndexOf(floatMenuOption)] = rightClickFloatMenuOption2;
						    }
					    }
				    }
			    }

			    Event.current.Use();
			    if (list4.Any())
			    {
				    Find.WindowStack.Add(new FloatMenu(list4));
			    }
		    }

		    for (int num10 = 0; num10 < ___gizmoGroups.Count; num10++)
		    {
			    ___gizmoGroups[num10].Clear();
			    SimplePool<List<Gizmo>>.Return(___gizmoGroups[num10]);
		    }

		    ___gizmoGroups.Clear();
		    ___firstGizmos.Clear();
		    ___tmpAllGizmos.Clear();

		    static List<Gizmo> FindMatchingGroup(List<List<Gizmo>> gizmoGroups, Gizmo toMatch)
		    {
			    for (int num11 = 0; num11 < gizmoGroups.Count; num11++)
			    {
				    if (gizmoGroups[num11].Contains(toMatch))
				    {
					    return gizmoGroups[num11];
				    }
			    }

			    return null;
		    }

		    void ProcessGizmoState(Gizmo giz, GizmoResult result, ref Gizmo mouseoverGiz)
		    {
			    if (result.State == GizmoState.Interacted || (result.State == GizmoState.OpenedFloatMenu &&
			                                                  giz.RightClickFloatMenuOptions.FirstOrDefault() == null))
			    {
				    interactedEvent = result.InteractEvent;
				    interactedGiz = giz;
			    }
			    else if (result.State == GizmoState.OpenedFloatMenu)
			    {
				    floatMenuGiz = giz;
			    }

			    if ((int) result.State >= 1)
			    {
				    mouseoverGiz = giz;
			    }
		    }
	    }
    }
    
    [HarmonyPatch(typeof(MainTabWindow), "SetInitialSizeAndPosition")]
    public class Patch_MainTabWindow
    {
	    static bool Prefix()
	    {
		    return false;
	    }
		
	    //Probably replace with transpile
	    static void Postfix(MainTabWindow __instance)
	    {
		    Vector2 initialSize = __instance.InitialSize;
		    __instance.windowRect = new Rect((UI.screenWidth - initialSize.x) / 2f, (UI.screenHeight - initialSize.y) / 2f, initialSize.x, initialSize.y);
		    __instance.windowRect = __instance.windowRect.Rounded();
            __instance.windowRect.x = RimGuiUtils.CenterOnScreenX((int)__instance.windowRect.width);
            __instance.windowRect.y = (UI.screenHeight - RimGuiExtensions.Settings.tabWindowBottomMargin) - __instance.windowRect.height;
	    }
	    
		/*[HarmonyTranspiler]
		static IEnumerable<CodeInstruction> MainTabWindow_Transpiler(IEnumerable<CodeInstruction> ins)
		{
			var codes = new List<CodeInstruction>(ins);
			Type t = typeof(UI);
			FieldInfo f1 = t.GetField("screenWidth");
			FieldInfo f2 = t.GetField("screenHeight");
			
			for (int i = 0; i < codes.Count; i++)
			{
				if (codes[i].LoadsField(f1))
				{
					i += 9;

					FieldInfo wR = AccessTools.Field(typeof(MainTabWindow), "windowRect");
					MethodInfo get_width = AccessTools.Method(typeof(Rect), "get_width");
					MethodInfo setX = AccessTools.Method(typeof(Rect), "set_x", new []{typeof(float)});
					MethodInfo center = AccessTools.Method(typeof(RimGuiUtils), "CenterOnScreenX", new []{typeof(int)});
					
					yield return new CodeInstruction(OpCodes.Ldarg_0);
					yield return new CodeInstruction(OpCodes.Ldflda, wR);
					yield return new CodeInstruction(OpCodes.Ldarg_0);
					yield return new CodeInstruction(OpCodes.Ldflda, wR);
					yield return new CodeInstruction(OpCodes.Call, get_width);
					yield return new CodeInstruction(OpCodes.Conv_I4);
					yield return new CodeInstruction(OpCodes.Call, center);
					yield return new CodeInstruction(OpCodes.Conv_I4);
					yield return new CodeInstruction(OpCodes.Call, setX);
					yield return new CodeInstruction(OpCodes.Nop);
				}
				else if (codes[i].LoadsField(f2))
				{
					i = codes.Count-1;
					
					FieldInfo wR = AccessTools.Field(typeof(MainTabWindow), "windowRect");
					FieldInfo screenHeight = AccessTools.Field(typeof(UI), "screenHeight");
					FieldInfo margin = AccessTools.Field(typeof(Settings), "tabWindowBottomMargin");
					MethodInfo get_settings = AccessTools.Method(typeof(RimGuiExtensions), "get_Settings");
					MethodInfo get_height = AccessTools.Method(typeof(Rect), "get_height");
					MethodInfo setY = AccessTools.Method(typeof(Rect), "set_y", new []{typeof(float)});
					
					yield return new CodeInstruction(OpCodes.Ldarg_0);
					yield return new CodeInstruction(OpCodes.Ldflda, wR);
					yield return new CodeInstruction(OpCodes.Ldsfld, screenHeight);
					yield return new CodeInstruction(OpCodes.Conv_I4);
					yield return new CodeInstruction(OpCodes.Call, get_settings);
					yield return new CodeInstruction(OpCodes.Ldfld, margin);
					yield return new CodeInstruction(OpCodes.Sub);
					yield return new CodeInstruction(OpCodes.Ldarg_0);
					yield return new CodeInstruction(OpCodes.Ldflda, wR);
					yield return new CodeInstruction(OpCodes.Call, get_height);
					yield return new CodeInstruction(OpCodes.Sub);
					yield return new CodeInstruction(OpCodes.Call, setY);
					yield return new CodeInstruction(OpCodes.Nop);
					yield return new CodeInstruction(OpCodes.Ret);
				}
				else
				{
					yield return codes[i];
				}
			}
		}*/
    }
}