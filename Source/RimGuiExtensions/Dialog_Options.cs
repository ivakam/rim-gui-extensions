using System;
using System.Collections.Generic;
using System.Reflection;
using HarmonyLib;
using RimWorld;
using RimWorld.Planet;
using UnityEngine;
using Verse;
using Text = Verse.Text;

namespace PorousBoat.RimGuiExtensions
{
	[StaticConstructorOnStartup]
	public class Dialog_Options : Window
	{
		public static Rect StaticRect = new Rect(0f, 0f, 900f, 550f);

		public static Listing_Standard row;

		public override Vector2 InitialSize => new Vector2(StaticRect.width, StaticRect.height);

		protected override float Margin => 20f;

		public Dialog_Options()
		{
			forcePause = false;
			absorbInputAroundWindow = false;
			closeOnCancel = true;
			soundAppear = SoundDefOf.CommsWindow_Open;
			soundClose = SoundDefOf.CommsWindow_Close;
			doCloseButton = false;
			doCloseX = true;
			draggable = true;
			drawShadow = false;
			preventCameraMotion = false;
			onlyOneOfTypeAllowed = true;
			resizeable = false;
		}

		public override void PreClose()
		{
			base.PreClose();
			RimGuiExtensions.Settings.Write();
		}

		public override void DoWindowContents(Rect inRect)
		{
			DoOptions(inRect);
		}

		public static void DoOptions(Rect inRect)
		{
			Rect rect = GenUI.ContractedBy(inRect, 6f);
			Settings Set = RimGuiExtensions.Settings;
			Widgets.DrawMenuSection(inRect);
			Listing_Standard val = new Listing_Standard();
			val.ColumnWidth = (rect.width - 34f) / 3f;
			row = val;
			row.Begin(rect);
			
			Text.Font = (GameFont) 2;
			row.Label(Translator.Translate("BetterUiSettings"), -1f, (string) null);
			Text.Font = (GameFont) 1;
			
			row.GapLine(12f);
			
			TaggedString s = Translator.Translate("BetterUiResetAll");
			if (row.ButtonDebug(s, false))
			{
				Set.ResetSettings();
			}

			Text.Font = (GameFont) 2;
			row.Label(Translator.Translate("BetterUiDock"), -1f, (string) null);
			Text.Font = (GameFont) 1;

			s = Translator.Translate("dockHeight");
			row.LabelDouble(s, Set.dockHeight.ToString() ?? "", (string) null);
			Set.dockHeight = (int) row.Slider(Set.dockHeight, 20, 250);

			s = Translator.Translate("dockPadding");
			row.LabelDouble(s, Set.dockPadding.ToString() ?? "", (string) null);
			Set.dockPadding = (int) row.Slider(Set.dockPadding, 0, 48);

			s = Translator.Translate("dockOffsetY");
			row.LabelDouble(s, Set.dockOffsetY.ToString() ?? "", (string) null);
			Set.dockOffsetY = (int) row.Slider(Set.dockOffsetY, 0, 150);

			row.GapLine(12f);

			Text.Font = (GameFont) 2;
			row.Label(Translator.Translate("BetterUiButtons"), -1f, (string) null);
			Text.Font = (GameFont) 1;

			s = Translator.Translate("mainButtonWidth");
			row.LabelDouble(s, Set.mainButtonWidth.ToString() ?? "", (string) null);
			Set.mainButtonWidth = (int) row.Slider(Set.mainButtonWidth, 16, 256);

			s = Translator.Translate("mainBottomMargin");
			row.LabelDouble(s, Set.mainBottomMargin.ToString() ?? "", (string) null);
			Set.mainBottomMargin = (int) row.Slider(Set.mainBottomMargin, 0, 48);

			s = Translator.Translate("mainButtonMargin");
			row.LabelDouble(s, Set.mainButtonMargin.ToString() ?? "", (string) null);
			Set.mainButtonMargin = (int) row.Slider(Set.mainButtonMargin, 0, 40);

			row.NewColumn();
			
			Text.Font = (GameFont) 2;
			row.Label(Translator.Translate("BetterUiIcons"), -1f, (string) null);
			Text.Font = (GameFont) 1;

			s = Translator.Translate("iconOffset");
			row.LabelDouble(s, Set.iconOffset.ToString() ?? "", (string) null);
			Set.iconOffset = (int) row.Slider(Set.iconOffset, 0, 48);

			s = Translator.Translate("iconSize");
			row.LabelDouble(s, Set.iconSize.ToString() ?? "", (string) null);
			Set.iconSize = (int) row.Slider(Set.iconSize, 16, 128);

			row.GapLine(12f);
			
			Text.Font = (GameFont) 2;
			row.Label(Translator.Translate("BetterUiGizmos"), -1f, (string) null);
			Text.Font = (GameFont) 1;

			s = Translator.Translate("gizmoStartX");
			row.LabelDouble(s, Set.gizmoStartX.ToString() ?? "", (string) null);
			Set.gizmoStartX = (int) row.Slider(Set.gizmoStartX, 10, 350);

			s = Translator.Translate("gizmoHeight");
			row.LabelDouble(s, Set.gizmoHeight.ToString() ?? "", (string) null);
			Set.gizmoHeight = (int) row.Slider(Set.gizmoHeight, 16, 256);
			
			s = Translator.Translate("gizmoRightMargin");
			row.LabelDouble(s, Set.gizmoRightMargin.ToString() ?? "", (string) null);
			Set.gizmoRightMargin = (int) row.Slider(Set.gizmoRightMargin, 16, 256);

			((Listing) row).GapLine(12f);

			Text.Font = ((GameFont) 2);
			row.Label(Translator.Translate("BetterUiMisc"), -1f, (string) null);
			Text.Font = ((GameFont) 1);

			s = Translator.Translate("useColorForIcons");
			TaggedString s2 = Translator.Translate("useColorForIconsDesc");
			row.CheckboxLabeled(s, ref Set.useColorForIcons, s2);

			s = Translator.Translate("useBorderForSmallIcons");
			s2 = Translator.Translate("useBorderForSmallIconsDesc");
			row.CheckboxLabeled(s, ref Set.useBorderForSmallIcons, s2);
			
			s = Translator.Translate("alwaysSortMinimizedLast");
			s2 = Translator.Translate("alwaysSortMinimizedLastDesc");
			row.CheckboxLabeled(s, ref Set.alwaysSortMinimizedLast, s2);

			row.End();
		}
	}
}